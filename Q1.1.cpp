#include<iostream>

using namespace std;

int comparisons = 0;


void printArray(int* array, int size){
    for (int i = 0; i<size; i++)
        cout<<array[i]<<" ";
    cout<<endl;
}


void insertionSort(int* array, int size){
    int key,j,i;
    for (i = 1; i < size; i++){
        key = array[i];
        j = i - 1;
        while (j>=0&&array[j]>key){
            array[j+1] = array[j];
            j--;
            comparisons++;
        }
        array[j+1] = key;
    }
}

int main(){
    int s;
    int* array;
    cout<<"Enter the no. of elements: ";
    cin>>s;
    array = new int [s];
    for(int i = 0; i<s; i++){
        cout<<"Enter element no. "<<i+1<<": ";
        cin>>array[i];
    }
    insertionSort(array, s);
        cout<<"Array after sort: ";
    printArray(array, s);
    cout<<"\nNumber of Comparisions: "<<comparisons<<endl;
}
