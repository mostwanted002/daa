#include <cstdlib> 
#include <iostream> 
using namespace std; 

int comparisons = 0;

int partition(int arr[], int low, int high){ 
    int pivot = arr[high]; 
    int i = (low - 1); 
    for (int j = low; j <= high - 1; j++){ 
        if (arr[j] <= pivot){
            i++;
            comparisons++;
            swap(arr[i], arr[j]);
        } 
    } 
    swap(arr[i + 1], arr[high]); 
    return (i + 1); 
}

int partition_r(int arr[], int low, int high){ 
    srand(time(NULL));
    int random = low + rand() % (high - low); 
    swap(arr[random], arr[high]);   
    return partition(arr, low, high); 
}

void quickSort(int arr[], int low, int high) {
    if (low < high){
        int pi = partition_r(arr, low, high);
        quickSort(arr, low, pi - 1); 
        quickSort(arr, pi + 1, high); 
    }
}

void printArray(int arr[], int size) 
{ 
    for (int i = 0; i < size; i++) 
        cout<<arr[i]<<" "; 
    cout<<endl; 
}

int main() 
{ 
    int *array; 
    int n;
    cout<<"Enter no. of elements: ";
    cin>>n;
    array = new int[n];
    for(int i = 0; i < n; i++){
        cout<<"Enter element number "<<i+1<<": ";
        cin>>array[i];
    }
    quickSort(array, 0, n - 1); 
    cout<<"Sorted array: \n"; 
    printArray(array, n);
    cout<<"\nNumber of Comparisions: "<<comparisons<<endl;
    return 0; 
} 
