#include<iostream>
#include<stdlib.h>

using namespace std;

void bubbleSort(int *array, int size);
void printArray(int *array, int size);

int main(){
    int *array, size;
    cout<<"Enter number of Elements: ";
    cin>>size;
    array = new int [size];
    for(int i = 0; i < size; i++){
        cout<<"Enter element no. "<<i+1<<": ";
        cin>>array[i];
    }
    bubbleSort(array, size);
    return 0;
}

void bubbleSort(int *array, int size){
    int comparision = 0;
    for(int i = 0; i<size-1; i++){
        for (int j = 0; j < (size - 1 - i); j++){
            comparision++;
            if (array[j]>array[j+1]){
                int temp = array[j];
                array [j] = array [j+1];
                array[j+1] = temp;
            }
        }
        cout<<"Array after Pass "<<i+1<<": ";
        printArray(array, size);
    }
    cout<<"Total no. of comparision: "<<comparision<<endl;
}

void printArray(int *array, int size){
    for(int i = 0; i < size; i++)
        cout<<array[i]<<" ";
    cout<<endl;
}
