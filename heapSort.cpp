#include<iostream>
#include<stdlib.h>
#include<math.h>

using namespace std;

class Node{
    Node* left;
    Node* right;
    int data;
public:
    Node(){
        this->left = 0;
        this->right = 0;
        this->data = 0;
    }
    Node(int data){
        this->left =0;
        this->right = 0;
        this->data = data;
    }
    int getData(){
        return this->data;
    }
    Node* getLeft(){
        return this->left;
    }
    Node* getRight(){
        return this->right;
    }
    void setRight(Node* node){
        this->right = node;
    }
    void setLeft(Node* node){
        this->left = node;
    }
};

class Heap{
    Node* root;
    int count, arrayIndex;
public:
    Heap(){
        this->root = 0;
        this->count = this->arrayIndex = 0;
    }
    Heap(int count){
        this->root = 0;
        this->count = count;
        this->arrayIndex = 0;
    }
    void setChilds(Node* node, int* array){
        if(arrayIndex<count){
            node->setLeft(new Node(array[arrayIndex]));
            arrayIndex++;
        }
        else
            node->setLeft = 0;
        if(arrayIndex<count){
            node->setRight(new Node(array[arrayIndex]);
            arrayIndex++;
        }
        else
            node->setRight = 0;
    }
    void buildHeap(int* array){
        this->root = new Node(array[0]);
        Node* temp = root;
        arrayIndex++;
        while(arrayIndex < count){
            setChilds(temp, array);
            temp = temp->getLeft();
            setChilds(temp, array);
            temp = temp->getRight();
            setChilds(temp, array);
            temp = temp->getLeft();
        }
    }
    void maxHeap(){
        //TODO
    }
    void heapSort(){
        //TODO
    }
};


int main(){
    return 0;
}
