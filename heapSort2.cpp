#include <iostream> 
  
using namespace std; 

int temp, comparisions = 0;

void heapify(int arr[], int n, int i) 
{ 
    int largest = i;
    int l = 2*i + 1; 
    int r = 2*i + 2; 
    if (l < n && arr[l] > arr[largest]){
        largest = l;
        comparisions++;
    }
    if (r < n && arr[r] > arr[largest]){
        largest = r;
        comparisions++;
    }
    if (largest != i){ 
        temp = arr[i]; 
        arr[i] = arr[largest];
        arr[largest] = temp;
        heapify(arr, n, largest); 
    } 
} 
void heapSort(int arr[], int n){ 
    for (int i = n / 2 - 1; i >= 0; i--) 
        heapify(arr, n, i);
    for (int i=n-1; i>=0; i--){ 
        temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;
        heapify(arr, i, 0); 
    } 
} 
  
void printArray(int arr[], int n) 
{ 
    for (int i=0; i<n; ++i) 
        cout << arr[i] << " "; 
    cout << "\n"; 
} 
  
int main() 
{ 
    int* array;
    int n;
    cout<<"Enter the number of Elements: ";
    cin>>n;
    array = new int[n];
    for(int i = 0; i < n; i++){
        cout<<"Enter element number "<<i+1<<": ";
        cin>>array[i];
    }
    heapSort(array, n); 
    cout << "Sorted array is: \n"; 
    printArray(array, n); 
    cout<<"Number of comparisions: "<<comparisions<<"\n";
} 
