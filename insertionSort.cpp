#include<iostream>

using namespace std;

void insertionSort(int *array, int size);
void printArray(int *array, int size);

void printArray(int *array, int size){
    for(int i = 0; i < size; i++)
        cout<<array[i]<<" ";
    cout<<endl;
}

int main(){
    int *array, size;
    cout<<"Enter number of Elements: ";
    cin>>size;
    array = new int [size];
    for(int i = 0; i < size; i++){
        cout<<"Enter element no. "<<i+1<<": ";
        cin>>array[i];
    }
    insertionSort(array, size);
    return 0;
}

void insertionSort(int *array, int size){
    int comparision = 0;    
    int key,j,i;
    for (i = 1; i < size; i++)
    {
        key = array[i];
        j = i - 1;
        while (j>=0&&array[j]>key)
        {
            comparision++;
            array[j+1] = array[j];
            j--;
        }
        array[j+1] = key;
        cout<<"Array after Pass "<<i<<": ";
        printArray(array, size);
    }
    cout<<"Total no. of comparisions: "<<comparision<<endl;
}
