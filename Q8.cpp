#include<iostream> 
#include<list> 
  
using namespace std; 
  
class Graph 
{ 
    int V;
    list<int> *adj;    
public: 
    Graph(int V){ 
        this->V = V; 
        adj = new list<int>[V]; 
    } 
    void addEdge(int v, int w){ 
    adj[v].push_back(w);
    } 
    void DFSUtil(int v, bool visited[]){
        visited[v] = true; 
        cout << v << " "; 
        list<int>::iterator i; 
        for (i = adj[v].begin(); i != adj[v].end(); ++i) 
            if (!visited[*i]) 
                DFSUtil(*i, visited);
    } 
    void DFS(int v) 
    { 
        bool *visited = new bool[V]; 
        for (int i = 0; i < V; i++) 
            visited[i] = false; 
        DFSUtil(v, visited); 
    }
};

int main(){ 
    int N,e,s;
    cout<<"Enter the number of Vertices: ";
    cin>>N;
    Graph g(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if(i==j)
                continue;
            else{
            cout<<"Edge between "<<i<<" - "<<j<<" ?(0 for 'NO', 1 for 'YES': ";
            cin>>e;
            if(e)
                g.addEdge(i, j);
            }
        }
    }
    cout<<"Enter the starting vertex: ";
    cin>>s;
    cout << "Following is Depth First Traversal "
         << "(starting from vertex "<<s<<") \n"; 
    g.DFS(s);
    return 0; 
}
